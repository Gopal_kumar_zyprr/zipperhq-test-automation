package com.zyprr.test;

public @interface Test {

	int priority();

	String[] groups();

}
