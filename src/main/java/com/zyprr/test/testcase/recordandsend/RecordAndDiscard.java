package com.zyprr.test.testcase.recordandsend;

import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.zyprr.test.base.factory.ZyprrPageFactory;
import com.zyprr.test.config.Config;
import com.zyprr.test.pageobject.common.ZipperHQHome;
import com.zyprr.test.testcase.abs.AbstractTest;

public class RecordAndDiscard extends AbstractTest {

	

	@Test(priority = 0)
	public void RecordAndDiscard() throws InterruptedException {

		ZyprrPageFactory.getInstance()
				.getPageObject(webDriver, ZipperHQHome.class)
				.xferTorecordandsendHomePage().recordVideo().discard();
		

	}

	@Override
	protected String getDataSheetName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String[] getFieldNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getDataPath() {
		// TODO Auto-generated method stub
		return null;
	}

	

}