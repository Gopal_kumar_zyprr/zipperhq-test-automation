package com.zyprr.test.testcase.abs;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.zyprr.test.config.Config;
import com.zyprr.test.utility.ExcelUtility;
import com.zyprr.test.utility.ZyprrUtilities;

import seleniumWebDriverMvnProject.WebDriverFactory;

public abstract class AbstractTest {
	public enum WebDriverType {FIREFOX,IE,CHROME};

	protected WebDriver webDriver = null;
	ExtentReports extent;
	

	// Added for Report
	public ExtentReports zyprrReport = null;
	public ExtentTest zyprrLogger = null;

	// End of addition for Report

	// private WebDriverType webDriverType = null;

	abstract protected String getDataSheetName();

	abstract protected String[] getFieldNames();

	abstract protected String getDataPath();

	/*abstract protected String getLeadDataPath();

	abstract protected String getContactDataPath();

	abstract protected String getCaseDataPath();

	abstract protected String getAccountDataPath();

	abstract protected String getReoppDataPath();
	
	abstract protected String getEstateDataPath();

	abstract protected String getNoteDataPath();

	abstract protected String getCallDataPath();

	abstract protected String getMeetingDataPath();

	abstract protected String getTaskDataPath();

	abstract protected String getProductDataPath();

	abstract protected String getInventoryDataPath();

	abstract protected String getActivityDataPath();
*/
	final static String dateFormat = new SimpleDateFormat("yyyy-MM-dd-hh-mm")
			.format(new Date());

	
	public Object[][] getData() throws Exception {
		ExcelUtility xlUtil = new ExcelUtility(getDataPath(),getDataSheetName());
		int rowCount = xlUtil.getNumberOfRows();
		System.out.println("value of rowcount=" +rowCount);
		String[] fieldNames = getFieldNames();
		int fieldCount = fieldNames.length;
		Object[][] data = new Object[rowCount - 1][fieldCount];
		Map<String, Object> currentMap = null;
		for (int i = 1; i < rowCount; i++) {
			currentMap = new HashMap<String, Object>();
			for (int j = 0; j < fieldNames.length; j++) {
				String currdata = xlUtil.getCellDataasstring(i, j);
				System.out.println("value of i=" +i);
				System.out.println("value of j=" +j);
				System.out.println("value of currdata=" +currdata);


				currentMap.put(fieldNames[j], xlUtil.getCellDataasstring(i, j));
			}
			data[i - 1] = new Object[] { currentMap };
		}

		return data;
	}

//	public Object[][] getLeadData() throws Exception {
//		ExcelUtility xlUtil = new ExcelUtility(getLeadDataPath(),
//				getDataSheetName());
//		int rowCount = xlUtil.getNumberOfRows();
//		String[] fieldNames = getFieldNames();
//		int fieldCount = fieldNames.length;
//		Object[][] data = new Object[rowCount - 1][fieldCount];
//		Map<String, Object> currentMap = null;
//		for (int i = 1; i < rowCount; i++) {
//			currentMap = new HashMap<String, Object>();
//			for (int j = 0; j < fieldNames.length; j++) {
//				currentMap.put(fieldNames[j], xlUtil.getCellDataasstring(i, j));
//			}
//			data[i - 1] = new Object[] { currentMap };
//		}
//
//		return data;
//	}
//
//	public Object[][] getContactData() throws Exception {
//		ExcelUtility xlUtil = new ExcelUtility(getContactDataPath(),
//				getDataSheetName());
//		System.out.println("xutil" +xlUtil);
//		int rowCount = xlUtil.getNumberOfRows();
//		String[] fieldNames = getFieldNames();
//		int fieldCount = fieldNames.length;
//		Object[][] data = new Object[rowCount - 1][fieldCount];
//		Map<String, Object> currentMap = null;
//		for (int i = 1; i < rowCount; i++) {
//			currentMap = new HashMap<String, Object>();
//			for (int j = 0; j < fieldNames.length; j++) {
//				currentMap.put(fieldNames[j], xlUtil.getCellDataasstring(i, j));
//			}
//			data[i - 1] = new Object[] { currentMap };
//		}
//
//		return data;
//	}
//
//	public Object[][] getCaseData() throws Exception {
//		ExcelUtility xlUtil = new ExcelUtility(getCaseDataPath(),
//				getDataSheetName());
//		int rowCount = xlUtil.getNumberOfRows();
//		String[] fieldNames = getFieldNames();
//		int fieldCount = fieldNames.length;
//		Object[][] data = new Object[rowCount - 1][fieldCount];
//		Map<String, Object> currentMap = null;
//		for (int i = 1; i < rowCount; i++) {
//			currentMap = new HashMap<String, Object>();
//			for (int j = 0; j < fieldNames.length; j++) {
//				currentMap.put(fieldNames[j], xlUtil.getCellDataasstring(i, j));
//			}
//			data[i - 1] = new Object[] { currentMap };
//		}
//
//		return data;
//	}
//
//	public Object[][] getAccountData() throws Exception {
//		ExcelUtility xlUtil = new ExcelUtility(getAccountDataPath(),
//				getDataSheetName());
//		int rowCount = xlUtil.getNumberOfRows();
//		String[] fieldNames = getFieldNames();
//		int fieldCount = fieldNames.length;
//		Object[][] data = new Object[rowCount - 1][fieldCount];
//		Map<String, Object> currentMap = null;
//		for (int i = 1; i < rowCount; i++) {
//			currentMap = new HashMap<String, Object>();
//			for (int j = 0; j < fieldNames.length; j++) {
//				currentMap.put(fieldNames[j], xlUtil.getCellDataasstring(i, j));
//			}
//			data[i - 1] = new Object[] { currentMap };
//		}
//
//		return data;
//	}
//
//	public Object[][] getReoppData() throws Exception {
////		System.out.println("dpath" +getOpportunityDataPath());
////		System.out.println("sname" +getDataSheetName());
//		ExcelUtility xlUtil = new ExcelUtility(getReoppDataPath(),
//				getDataSheetName());
//		int rowCount = xlUtil.getNumberOfRows();
//		String[] fieldNames = getFieldNames();
//		int fieldCount = fieldNames.length;
//		Object[][] data = new Object[rowCount - 1][fieldCount];
//		Map<String, Object> currentMap = null;
//		for (int i = 1; i < rowCount; i++) {
//			currentMap = new HashMap<String, Object>();
//			for (int j = 0; j < fieldNames.length; j++) {
//				currentMap.put(fieldNames[j], xlUtil.getCellDataasstring(i, j));
//			}
//			data[i - 1] = new Object[] { currentMap };
//		}
//
//		return data;
//	}
//	
//	public Object[][] getEstateData() throws Exception {
////		System.out.println("dpath" +getOpportunityDataPath());
////		System.out.println("sname" +getDataSheetName());
//		ExcelUtility xlUtil = new ExcelUtility(getEstateDataPath(),
//				getDataSheetName());
//		int rowCount = xlUtil.getNumberOfRows();
//		String[] fieldNames = getFieldNames();
//		int fieldCount = fieldNames.length;
//		Object[][] data = new Object[rowCount - 1][fieldCount];
//		Map<String, Object> currentMap = null;
//		for (int i = 1; i < rowCount; i++) {
//			currentMap = new HashMap<String, Object>();
//			for (int j = 0; j < fieldNames.length; j++) {
//				currentMap.put(fieldNames[j], xlUtil.getCellDataasstring(i, j));
//			}
//			data[i - 1] = new Object[] { currentMap };
//		}
//
//		return data;
//	}
//
//	public Object[][] getNoteData() throws Exception {
//		ExcelUtility xlUtil = new ExcelUtility(getNoteDataPath(),
//				getDataSheetName());
//		int rowCount = xlUtil.getNumberOfRows();
//		String[] fieldNames = getFieldNames();
//		int fieldCount = fieldNames.length;
//		Object[][] data = new Object[rowCount - 1][fieldCount];
//		Map<String, Object> currentMap = null;
//		for (int i = 1; i < rowCount; i++) {
//			currentMap = new HashMap<String, Object>();
//			for (int j = 0; j < fieldNames.length; j++) {
//				currentMap.put(fieldNames[j], xlUtil.getCellDataasstring(i, j));
//			}
//			data[i - 1] = new Object[] { currentMap };
//		}
//
//		return data;
//	}
//
//	public Object[][] getCallData() throws Exception {
//		ExcelUtility xlUtil = new ExcelUtility(getCallDataPath(),
//				getDataSheetName());
//		int rowCount = xlUtil.getNumberOfRows();
//		String[] fieldNames = getFieldNames();
//		int fieldCount = fieldNames.length;
//		Object[][] data = new Object[rowCount - 1][fieldCount];
//		Map<String, Object> currentMap = null;
//		for (int i = 1; i < rowCount; i++) {
//			currentMap = new HashMap<String, Object>();
//			for (int j = 0; j < fieldNames.length; j++) {
//				currentMap.put(fieldNames[j], xlUtil.getCellDataasstring(i, j));
//			}
//			data[i - 1] = new Object[] { currentMap };
//		}
//
//		return data;
//	}
//
//	public Object[][] getMeetingData() throws Exception {
//		ExcelUtility xlUtil = new ExcelUtility(getMeetingDataPath(),
//				getDataSheetName());
//		int rowCount = xlUtil.getNumberOfRows();
//		String[] fieldNames = getFieldNames();
//		int fieldCount = fieldNames.length;
//		Object[][] data = new Object[rowCount - 1][fieldCount];
//		Map<String, Object> currentMap = null;
//		for (int i = 1; i < rowCount; i++) {
//			currentMap = new HashMap<String, Object>();
//			for (int j = 0; j < fieldNames.length; j++) {
//				currentMap.put(fieldNames[j], xlUtil.getCellDataasstring(i, j));
//			}
//			data[i - 1] = new Object[] { currentMap };
//		}
//
//		return data;
//	}
//
//	public Object[][] getTaskData() throws Exception {
//		ExcelUtility xlUtil = new ExcelUtility(getTaskDataPath(),
//				getDataSheetName());
//		int rowCount = xlUtil.getNumberOfRows();
//		String[] fieldNames = getFieldNames();
//		int fieldCount = fieldNames.length;
//		Object[][] data = new Object[rowCount - 1][fieldCount];
//		Map<String, Object> currentMap = null;
//		for (int i = 1; i < rowCount; i++) {
//			currentMap = new HashMap<String, Object>();
//			for (int j = 0; j < fieldNames.length; j++) {
//				currentMap.put(fieldNames[j], xlUtil.getCellDataasstring(i, j));
//			}
//			data[i - 1] = new Object[] { currentMap };
//		}
//
//		return data;
//	}
//
//	public Object[][] getProductData() throws Exception {
//		ExcelUtility xlUtil = new ExcelUtility(getProductDataPath(),
//				getDataSheetName());
//		int rowCount = xlUtil.getNumberOfRows();
//		String[] fieldNames = getFieldNames();
//		int fieldCount = fieldNames.length;
//		Object[][] data = new Object[rowCount - 1][fieldCount];
//		Map<String, Object> currentMap = null;
//		for (int i = 1; i < rowCount; i++) {
//			currentMap = new HashMap<String, Object>();
//			for (int j = 0; j < fieldNames.length; j++) {
//				currentMap.put(fieldNames[j], xlUtil.getCellDataasstring(i, j));
//			}
//			data[i - 1] = new Object[] { currentMap };
//		}
//		return data;
//	}
//
//	public Object[][] getInventoryData() throws Exception {
//		ExcelUtility xlUtil = new ExcelUtility(getInventoryDataPath(),
//				getDataSheetName());
//		int rowCount = xlUtil.getNumberOfRows();
//		String[] fieldNames = getFieldNames();
//		int fieldCount = fieldNames.length;
//		Object[][] data = new Object[rowCount - 1][fieldCount];
//		Map<String, Object> currentMap = null;
//		for (int i = 1; i < rowCount; i++) {
//			currentMap = new HashMap<String, Object>();
//			for (int j = 0; j < fieldNames.length; j++) {
//
//				currentMap.put(fieldNames[j], xlUtil.getCellDataasstring(i, j));
//			}
//			data[i - 1] = new Object[] { currentMap };
//		}
//		return data;
//	}
//
//	public Object[][] getActivityData() throws Exception {
//		ExcelUtility xlUtil = new ExcelUtility(getActivityDataPath(),
//				getDataSheetName());
//		int rowCount = xlUtil.getNumberOfRows();
//		String[] fieldNames = getFieldNames();
//		int fieldCount = fieldNames.length;
//		Object[][] data = new Object[rowCount - 1][fieldCount];
//		Map<String, Object> currentMap = null;
//		for (int i = 1; i < rowCount; i++) {
//			currentMap = new HashMap<String, Object>();
//			for (int j = 0; j < fieldNames.length; j++) {
//				currentMap.put(fieldNames[j], xlUtil.getCellDataasstring(i, j));
//			}
//			data[i - 1] = new Object[] { currentMap };
//		}
//
//		return data;
//	}

	@BeforeClass
	@Parameters("browser")
	public void loadWebDriver(String browser) {

		System.out.println("loadWebDriver " + browser);
		if ("ie".equalsIgnoreCase(browser)) {
			webDriver = WebDriverFactory.getInstance().getIEDriver();
		} else if ("chrome".equalsIgnoreCase(browser)) {
			webDriver = WebDriverFactory.getInstance().getChoromeDriver();}
//		} else if ("phantomjs".equalsIgnoreCase(browser)) {
//			webDriver = WebDriverFactory.getInstance().getPhantomJSDriver();
//		//	webDriver.manage().window().setSize(new Dimension(1366,768));
//				}
		else {
			webDriver = WebDriverFactory.getInstance().getFireFoxDriver();
			System.out.println("hello123");
		}
		// Added for Report
		/*String css = ".report-name { padding-left: 10px; } .report-name > img { float: left;height: 90%;margin-left: 30px;margin-top: 2px;width: auto; }";
		*/
		//new ExtentReports(Config().reportName());
		zyprrReport = new ExtentReports(Config.getInstance().getReportFolder()
				+ "Report-" + dateFormat + ".html", false);
		System.out.println("Dateformate is="+dateFormat);
		/*zyprrReport.config().reportHeadline("http://www.soais.com/sites/all/themes/soais/logo.png").insertCustomStyles(css);
		*/

		// End of addition for Report

	}

	@AfterMethod
	public void takeScreenShot(ITestResult testResult) throws IOException {
		
		
		if (testResult.getStatus() == ITestResult.FAILURE) {
//			if(!(webDriver instanceof PhantomJSDriver)){
				zyprrLogger.log(LogStatus.FAIL,"Assertion failed for "+testResult.getTestName()+zyprrLogger
						.addScreenCapture(ZyprrUtilities.takeDriverSnapShotForReport(
								webDriver, webDriver.getTitle())));	
//			}
			
			
			/*StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			Throwable cause = testResult.getThrowable();
			if (null != cause) {
				cause.printStackTrace(pw);

				zyprrLogger.log(LogStatus.ERROR, sw.getBuffer().toString());
			}*/
			
			
		}
		
	}

}