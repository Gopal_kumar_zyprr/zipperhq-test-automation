package com.zyprr.test.testcase.admin;

import java.awt.AWTException;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.zyprr.test.base.factory.ZyprrPageFactory;
import com.zyprr.test.config.Config;
import com.zyprr.test.pageobject.common.ZipperHQHome;
import com.zyprr.test.testcase.abs.AbstractTest;

public class ProfileDetails extends AbstractTest {

	private Map<String, Object> inputData = null;

	private static final String[] fieldNames = { };

	private static final String sheetName = "login";
	private static ProfileDetails staticInstance = new ProfileDetails(null);

	@Factory(dataProvider = "dataMethod")
	public ProfileDetails(Map<String, Object> inputData) {
		this.inputData = inputData;
	}

	@DataProvider
	public static Object[][] dataMethod() throws Exception {
		return staticInstance.getData();
	}

	@Test(priority = 0)
	public void ProfileDetails() throws InterruptedException {

		ZyprrPageFactory.getInstance()
				.getPageObject(webDriver, ZipperHQHome.class)
				.xferToAdminHomePage().changePassword();
		

	}

	@Override
	protected String getDataPath() {

		return Config.getInstance().getCalenderDataPath();
	}

	@Override
	protected String getDataSheetName() {
		// TODO Auto-generated method stub
		return sheetName;
	}

	@Override
	protected String[] getFieldNames() {
		// TODO Auto-generated method stub
		return fieldNames;
	}

	

}