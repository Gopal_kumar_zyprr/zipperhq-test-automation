package com.zyprr.test.testcase.contact;

import java.util.Map;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.zyprr.test.base.factory.ZyprrPageFactory;
import com.zyprr.test.config.Config;
import com.zyprr.test.pageobject.common.ZipperHQHome;
import com.zyprr.test.pageobject.contact.ContactDetails;
import com.zyprr.test.pageobject.contact.ContactsHome;
import com.zyprr.test.testcase.abs.AbstractTest;

public class AddContact extends AbstractTest {

	private Map<String, Object> inputData = null;

	private static final String[] fieldNames = {"FirstName","LastName","Email","Phone", "Company"
			};

	private static final String sheetName = "addcontact";
	private static AddContact staticInstance = new AddContact(null);

	@Factory(dataProvider = "dataMethod")
	public AddContact(Map<String, Object> inputData) {
		this.inputData = inputData;
	}

	@DataProvider
	public static Object[][] dataMethod() throws Exception {
		return staticInstance.getData();
	}

	@Test(priority = 0)
	public void addContactTest() throws InterruptedException {

		ZyprrPageFactory.getInstance()
				.getPageObject(webDriver, ZipperHQHome.class)
				.xferToContactHomePage()
				.createNewContact(inputData, fieldNames);

}

	@Override
	protected String getDataPath() {

		return Config.getInstance().getContactDataPath();
	}

	@Override
	protected String getDataSheetName() {
		// TODO Auto-generated method stub
		return sheetName;
	}

	@Override
	protected String[] getFieldNames() {
		// TODO Auto-generated method stub
		return fieldNames;
	}

	

}