package com.zyprr.test.testcase.contact;

import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;
import com.zyprr.test.config.*;
import com.relevantcodes.extentreports.LogStatus;
import com.zyprr.test.base.factory.ZyprrPageFactory;
import com.zyprr.test.config.Config;
import com.zyprr.test.pageobject.common.ZipperHQHome;
import com.zyprr.test.pageobject.contact.ContactsHome;
import com.zyprr.test.testcase.abs.AbstractTest;

public class ContactSearch extends AbstractTest {

	private Map<String, Object> inputData = null;
	private static final String[] fieldNames = {"filterContactsTextBox"};

	private static final String sheetName = "contactsearch";
	private static ContactSearch staticInstance = new ContactSearch(null);

	@Factory(dataProvider = "dataMethod")
	public ContactSearch(Map<String, Object> inputData) {
		
		this.inputData = inputData;
	}

	@DataProvider
	public static Object[][] dataMethod() throws Exception {
		return staticInstance.getData();
	}

	@Test(priority = 0)
	public void ContactSearchTest() throws InterruptedException {
		
		// Added for Report
		zyprrLogger = zyprrReport.startTest("ContactSearchTest");

		zyprrLogger.log(LogStatus.INFO, "Start of  Contact Search data");
		// End of addition for Report
		System.out.println("input data =" +inputData);
     	String firstName = ((String) inputData.get(fieldNames[0]));

		ContactsHome contactsHome =	ZyprrPageFactory.getInstance()
		.getPageObject(webDriver, ZipperHQHome.class).xferToContactHomePage1()
				.searchContacts(firstName);

	//	Assert.assertTrue(contactsHome.getFirstNameInList().getText()
	//			.contains(firstName));

		zyprrLogger.log(LogStatus.INFO, "End of Contact Search  data");
		zyprrLogger.log(LogStatus.INFO, "Verification Start");


	}

	@AfterClass
	public void tearDown() {
		zyprrReport.endTest(zyprrLogger);
		zyprrReport.flush();

	}

	@Override
	protected String getDataPath() {
		return Config.getInstance().getContactDataPath();
	}

	@Override
	protected String getDataSheetName() {
		// TODO Auto-generated method stub
		return sheetName;
	}

	@Override
	protected String[] getFieldNames() {
		// TODO Auto-generated method stub
		return fieldNames;
	}

	

}