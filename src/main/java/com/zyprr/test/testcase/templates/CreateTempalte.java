package com.zyprr.test.testcase.templates;

import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.zyprr.test.base.factory.ZyprrPageFactory;
import com.zyprr.test.config.Config;
import com.zyprr.test.pageobject.common.ZipperHQHome;
import com.zyprr.test.testcase.abs.AbstractTest;

public class CreateTempalte extends AbstractTest {

	private Map<String, Object> inputData = null;

	private static final String[] fieldNames = { };

	private static final String sheetName = "login";
	private static CreateTempalte staticInstance = new CreateTempalte(null);

	@Factory(dataProvider = "dataMethod")
	public CreateTempalte(Map<String, Object> inputData) {
		this.inputData = inputData;
	}

	@DataProvider
	public static Object[][] dataMethod() throws Exception {
		return staticInstance.getData();
	}

	@Test(priority = 0)
	public void CreateTempalte() throws InterruptedException {

		ZyprrPageFactory.getInstance()
				.getPageObject(webDriver, ZipperHQHome.class)
				.xferToTemplateHomePage();
		

	}

	@Override
	protected String getDataPath() {

		return Config.getInstance().getCalenderDataPath();
	}

	@Override
	protected String getDataSheetName() {
		// TODO Auto-generated method stub
		return sheetName;
	}

	@Override
	protected String[] getFieldNames() {
		// TODO Auto-generated method stub
		return fieldNames;
	}

	

}