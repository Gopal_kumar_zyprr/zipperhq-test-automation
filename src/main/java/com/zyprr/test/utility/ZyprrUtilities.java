package com.zyprr.test.utility;


import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.zyprr.test.config.Config;

public class ZyprrUtilities {

	public static void takeDriverSnapShot(WebDriver driver,
			String screenSnapshotName) {

		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd-HH-mm-ss");
		screenSnapshotName = screenSnapshotName + "-"
				+ dateFormat.format(new Date());
		File browserFile = new File(Config.getInstance().getScreenshotsFolder()
				+ screenSnapshotName + ".png");
		snapshotBrowser((TakesScreenshot) driver, screenSnapshotName,
				browserFile);
	}

	public static String takeDriverSnapShotForReport(WebDriver driver,
			String screenSnapshotName) {

		try {

			TakesScreenshot ts = (TakesScreenshot) driver;
			File sourceFile = ts.getScreenshotAs(OutputType.FILE);

			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"yyyy-MM-dd-HH-mm-ss");
			screenSnapshotName = screenSnapshotName + "-"
					+ dateFormat.format(new Date());

			/*
			 * String destinationFolder =
			 * "D:\\ZyprrTestAutomation\\seleniumWebDriverMvnProj\\screenshots\\"
			 * + screenSnapshotName + ".png";
			 */

			

			String destinationFolder = Config.getInstance()
					.getScreenshotsFolder() + screenSnapshotName + ".png";

			File destinationFile = new File(destinationFolder);
			FileUtils.copyFile(sourceFile, destinationFile);
			System.out.println("Screenshot taken");
			destinationFolder = "." + destinationFolder;
			return destinationFolder;

		} catch (Exception e) {
			System.out.println("Exception while taking screenshots: "
					+ screenSnapshotName + ": " + e.getMessage());
			return e.getMessage();
		}
	}

	private static void snapshotBrowser(TakesScreenshot driver,
			String screenSnapshotName, File browserFile) {
		try {

			File scrFile = driver.getScreenshotAs(OutputType.FILE);
			FileUtils.deleteQuietly(browserFile);
			FileUtils.moveFile(scrFile, browserFile);

		} catch (Exception e) {
			System.out.println("Could not create browser snapshot: "
					+ screenSnapshotName + ": " + e.getMessage());
		}
	}

	private static void setClipboardData(String string) {
		StringSelection stringSelection = new StringSelection(string);
		Toolkit.getDefaultToolkit().getSystemClipboard()
				.setContents(stringSelection, null);
	}

	public static void getClipboardData(String string) throws Exception {
		// native key strokes for CTRL, V and ENTER keys
		setClipboardData(string);
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);

		Thread.sleep(2000);

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}

	/**
	 * @param verification
	 *            : Label name;
	 * @param status
	 *            : Pass/ Fail;
	 * @param firstIdentifier
	 *            : First name of Lead;
	 * @param secondIdentifier
	 *            : Last name of Lead;
	 * @param actualValue
	 *            : Actual value of the field from the screen;
	 * @param expectedValue
	 *            : Value of the field from the excel sheet;
	 */
	public static void writeReportLog(String verification, String status,
			String firstIdentifier, String secondIdentifier,
			String actualValue, String expectedValue) {

		String mark = "";
		if (status.equals("PASS")) {
			Reporter.log("<font color=\"green\">");
		} else {
			Reporter.log("<font color=\"red\">");
			mark = "<mark>";

		}

		Reporter.log("Verification for field (" + verification + "): " + status
				+ ", for(" + firstIdentifier + " " + secondIdentifier + ").");
		Reporter.log("<br>");
		Reporter.log("Actual Value of field (" + verification + ") is : "
				+ mark + actualValue);
		Reporter.log("<br>");
		Reporter.log("Expected Value of field (" + verification + ") is : "
				+ mark + expectedValue);
		Reporter.log("</font>");
		Reporter.log("<hr>");
	}

	public static void writeExtentReportLog(String verification, String status,
			String firstIdentifier, String secondIdentifier,
			String actualValue, String expectedValue,ExtentTest zyprrLogger,WebDriver driver) {
		
		
		zyprrLogger.log(LogStatus.FAIL,"Verification for field (" + verification + "): " + status
				+ ", for(" + firstIdentifier + " " + secondIdentifier + ") <br> "
						+ "Actual Value of field (" + verification + ")"
								+ " is :"+ actualValue+"<br>Expected Value of field "
										+ "(" + verification + ")"
												+ " is :"+ expectedValue+" "+
												zyprrLogger.addScreenCapture(ZyprrUtilities.takeDriverSnapShotForReport(
														driver, driver.getTitle())));
		
	}
}
