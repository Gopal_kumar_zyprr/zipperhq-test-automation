
package com.zyprr.test ;


import java.util.Map;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;
import com.zyprr.test.base.factory.ZyprrPageFactory;
import com.zyprr.test.config.Config;
import com.zyprr.test.pageobject.common.ZHQLoggin;
import com.zyprr.test.pageobject.common.ZipperHQHome;
import com.zyprr.test.testcase.abs.AbstractTest;

public class ZipperHQLoggin extends AbstractTest {
	private Map<String, Object> inputData = null;
	private static final String[] fieldNames = { "userName", "password", "rememberMe"};
	private static final String sheetName = "login";
//	private static final String LOGIN_URL1="login";
	private static final String LOGIN_URL="login";

	private static ZipperHQLoggin staticInstance = new ZipperHQLoggin(null);

	@Factory(dataProvider = "dataMethod")
	public ZipperHQLoggin(Map<String, Object> inputFromXl) {
		this.inputData = inputFromXl;
	}

	@DataProvider
	public static Object[][] dataMethod() throws Exception {
		return staticInstance.getData();
	}

	
	@Test(priority = 0, groups = { "login" })
	public void loginTest() throws InterruptedException {
	  	//webDriver.navigate().to(Config.getInstance().getBaseUrl() + LOGIN_URL);
	//	webDriver.manage().window().maximize();
		

		ZyprrPageFactory.getInstance()
				.getPageObject(webDriver, ZipperHQHome.class)
				.login1();
		zyprrLogger = zyprrReport.startTest("LogintTest");
		zyprrLogger.log(LogStatus.INFO, "Clicked on Login");
		ZHQLoggin loginPage = PageFactory.initElements(webDriver,
				ZHQLoggin.class);
		zyprrLogger.log(LogStatus.INFO, "loginPage=="+loginPage);
		
	}
	

		
//Added for Report
		@AfterClass
		public void tearDown() {
			zyprrReport.endTest(zyprrLogger);
			zyprrReport.flush();

		}
	// End of addition for Report

	@Override
	protected String getDataPath() {
		return Config.getInstance().getDataPath();
	}

	/*@Override
	protected String getLeadDataPath() {
//		return Config.getInstance().getLeadDataPath();
		return null;
	}

	@Override
	protected String getContactDataPath() {
		return null;
	}*/

	@Override
	protected String getDataSheetName() {
		return sheetName;
	}

	@Override
	protected String[] getFieldNames() {
		return fieldNames;
	}

	
}
