package com.zyprr.test.base.factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.zyprr.test.base.pageobject.BasePageObject;
import com.zyprr.test.base.pageobject.BasePhoneyPopupPageObject;
import com.zyprr.test.base.pageobject.BasePopupPageObject;
import com.zyprr.test.pageobject.util.PopupUtil;
import com.zyprr.test.pageobject.util.WaitUtil;

public class ZyprrPageFactory {	
	
	private static ZyprrPageFactory instance = new ZyprrPageFactory();
	
	private ZyprrPageFactory(){}
	
	public static ZyprrPageFactory getInstance(){
		return instance;
	}
	
	public <T extends BasePageObject> T getPageObject(WebDriver webDriver,Class<T> pageObjClass){
		T pageObj = PageFactory.initElements(webDriver, pageObjClass);
		pageObj.waitForPageLoad();
		return pageObj;
	}
	
	// this method is to be called for actual pop up windows which increase the count of windows on the screen
	public <T extends BasePopupPageObject> T getPopupPageObject(WebDriver webDriver,Class<T> pageObjClass){
		PopupUtil.getInstance().switchToPopupWindow(webDriver);
		T pageObj = PageFactory.initElements(webDriver, pageObjClass);
		pageObj.waitForPageLoad();
		return pageObj;
	}
	
	// this method is to be called for phoney pop up windows which are essentially a div on the parent page 
	// and give an illusion of pop up windows by being visible at some time and invisible at other.
	public <T extends BasePhoneyPopupPageObject> T getPhoneyPopupPageObject(WebDriver webDriver,Class<T> pageObjClass){		
		T pageObj = PageFactory.initElements(webDriver, pageObjClass);
		WaitUtil.getInstance().waitForElementTobeVisible(webDriver, pageObj.getParentElement());
		pageObj.waitForPageLoad();
		return pageObj;
	}
}
