package com.zyprr.test.base.pageobject;

import org.openqa.selenium.WebElement;

public interface PageIntf {
	public WebElement[] getPageLoadIndicatorElements();
	public void init();	
	public void waitForPageLoad();
}
