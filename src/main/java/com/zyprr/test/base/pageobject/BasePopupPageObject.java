package com.zyprr.test.base.pageobject;

import org.openqa.selenium.WebDriver;

abstract public class BasePopupPageObject extends BasePageObject {
	protected BasePopupPageObject(WebDriver webDriver) {
		super(webDriver);
	}
}

