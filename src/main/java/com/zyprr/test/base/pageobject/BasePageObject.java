package com.zyprr.test.base.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

abstract public class BasePageObject {
	protected WebDriver webDriver = null;

	protected BasePageObject(WebDriver webDriver) {
		this.webDriver = webDriver;
		try {
			init();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	abstract protected WebElement[] getPageLoadIndicatorElements();
	abstract protected void init();
	abstract public void waitForPageLoad();
}
