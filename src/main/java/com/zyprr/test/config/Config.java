package com.zyprr.test.config;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class Config {
	private static String DATA_PATH_KEY = "data.path";
	private static String LEAD_DATA_PATH_KEY = "lead.data.path";
	private static String CONTACT_DATA_PATH_KEY = "contact.data.path";
	private static String CONTACT_DATA_PATH_KEY1 = "contact.data.path1";
	private static String CASE_DATA_PATH_KEY = "case.data.path";
	private static String ACCOUNT_DATA_PATH_KEY = "account.data.path";
	private static String REOPP_DATA_PATH_KEY = "reopp.data.path";
	private static String ESTATE_DATA_PATH_KEY = "estate.data.path";
	private static String NOTE_DATA_PATH_KEY = "note.data.path";
	private static String CALL_DATA_PATH_KEY = "call.data.path";
	private static String MEETING_DATA_PATH_KEY = "meeting.data.path";
	private static String TASK_DATA_PATH_KEY = "task.data.path";
	private static String CALENDER_DATA_PATH_KEY = "calender.data.path";
	private static String SCREENSHOT_FOLDER_KEY = "screenshot.folder";
	private static String REPORT_FOLDER_KEY = "report.folder";
	private static String IEDRIVER_PATH_KEY = "ie.driver.path";
	private static String CHROMEDRIVER_PATH_KEY = "chrome.driver.path";
	private static String PHANTOMJS_PATH_KEY = "phantom.driver.path";
	private static String PRODUCT_DATA_PATH_KEY = "product.data.path";
	private static String INVENTORY_DATA_PATH_KEY = "inventory.data.path";
	private static String ACTIVITY_DATA_PATH_KEY = "activity.data.path";
	private static String EMAIL_DATA_PATH_KEY = "email.data.path";
	private static String MAIlCHIMP_DATA_PATH_KEY = "mailchimp.data.path";
	private static String MASSEMAIL_DATA_PATH_KEY = "massemail.data.path";
	
	private static String BASE_URL_KEY = "stephen.zenzuzu.com";
	

	private static Config instance = new Config();
	private Map<String, String> propMap = new HashMap<String, String>();

	public static Config getInstance() {
		return instance;
	}

	private Config() {
		ResourceBundle rsBndl = ResourceBundle.getBundle("config");
		Enumeration<String> keys = rsBndl.getKeys();
		String currentKey = null;
		while (keys.hasMoreElements()) {
			currentKey = keys.nextElement();
			propMap.put(currentKey, rsBndl.getString(currentKey));
		}
	}

	public String getDataPath() {
		return propMap.get(DATA_PATH_KEY);
	}

	public String getLeadDataPath() {
		return propMap.get(LEAD_DATA_PATH_KEY);
	}

	public String getContactDataPath() {
		return propMap.get(CONTACT_DATA_PATH_KEY);
	}
	
	public String getContactDataPath1() {
		return propMap.get(CONTACT_DATA_PATH_KEY1);
	}
	public String getCaseDataPath() {
		return propMap.get(CASE_DATA_PATH_KEY);
	}
	
	public String getAccountDataPath() {
		return propMap.get(ACCOUNT_DATA_PATH_KEY);
	}

	public String getReoppDataPath() {
		return propMap.get(REOPP_DATA_PATH_KEY);
	}
	
	public String getEstateDataPath() {
		return propMap.get(ESTATE_DATA_PATH_KEY);
	}

	public String getNoteDataPath() {
		return propMap.get(NOTE_DATA_PATH_KEY);
	}
	
	public String getCallDataPath() {
		return propMap.get(CALL_DATA_PATH_KEY);
	}
	
	public String getMeetingDataPath() {
		return propMap.get(MEETING_DATA_PATH_KEY);
	}
	
	public String getTaskDataPath() {
		return propMap.get(TASK_DATA_PATH_KEY);
	}
	
	public String getCalenderDataPath() {
		return propMap.get(CALENDER_DATA_PATH_KEY);
	}
	
	public String getScreenshotsFolder() {
		return propMap.get(SCREENSHOT_FOLDER_KEY);
	}

	public String getReportFolder() {
		return propMap.get(REPORT_FOLDER_KEY);
	}
	
	
	public String getIEDriverPath() {
		return propMap.get(IEDRIVER_PATH_KEY);
	}

	public String getProductDataPath() {
		return propMap.get(PRODUCT_DATA_PATH_KEY);
	}
	
	public String getInventoryDataPath() {
		return propMap.get(INVENTORY_DATA_PATH_KEY);
	}
	
	public String getMailChimplDataPath() {
		return propMap.get(MAIlCHIMP_DATA_PATH_KEY);
	}
	
	public String getMassEmailDataPath() {
		return propMap.get(MASSEMAIL_DATA_PATH_KEY);
	}
	
	public String getActivityDataPath() {
		return propMap.get(ACTIVITY_DATA_PATH_KEY);
	}
	
	public String getEmailDataPath() {
		return propMap.get(EMAIL_DATA_PATH_KEY);
	}
	
	public String getChromeDriverPath() {
		return propMap.get(CHROMEDRIVER_PATH_KEY);
	}
	public String getPhantomjsDriverPath() {
		return propMap.get(PHANTOMJS_PATH_KEY);
	}
	public String getBaseUrl(){
		return propMap.get(BASE_URL_KEY);
	}
}