package com.zyprr.test.pageobject.util;

import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.zyprr.test.constants.Constants;

public class PopupUtil {
	private static PopupUtil instance = new PopupUtil();
	
	private PopupUtil() {}
	
	public static PopupUtil getInstance(){
		return instance;
	} 
	
	public void switchToPopupWindow(WebDriver webDriver){
		WebDriverWait wait = new WebDriverWait(webDriver,Constants.WAIT_IN_SECONDS); 
		wait.until(new PopupExpectedCondition());
		String parentWindowHandle = webDriver.getWindowHandle();
		Set<String> allWindowHandles = webDriver.getWindowHandles();
		allWindowHandles.remove(parentWindowHandle);
		webDriver.switchTo().window(allWindowHandles.iterator().next());
	}
	
	public void switchToMainWindow(WebDriver webDriver){
		WebDriverWait wait = new WebDriverWait(webDriver,Constants.WAIT_IN_SECONDS); 
		wait.until(new PopupCloseCondition());
		String parentWindowHandle = webDriver.getWindowHandle();		
		webDriver.switchTo().window(parentWindowHandle);
	}
	
	
	private static class PopupExpectedCondition implements ExpectedCondition<Boolean>{

		public Boolean apply(WebDriver webDriver) {			
			System.out.println("inside PopupExpectedCondition, window count : " + webDriver.getWindowHandles().size() + " obj " + this + "window handles : " + webDriver.getWindowHandles());
			return Boolean.valueOf(webDriver.getWindowHandles().size() > 1);
		}
		
	}
	
	private static class PopupCloseCondition implements ExpectedCondition<Boolean>{

		public Boolean apply(WebDriver webDriver) {			
			System.out.println("inside PopupCloseCondition");
			return Boolean.valueOf(webDriver.getWindowHandles().size() == 1);
		}
		
	}
}
