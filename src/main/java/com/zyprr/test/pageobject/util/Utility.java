package com.zyprr.test.pageobject.util;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Utility {

	private static Utility instance = new Utility();
	
	private Utility() {}
	
	public static Utility getinstance() {
		
		return instance;
	}
	
	public boolean exists (WebDriver driver, String elementId){
		return driver.findElements(By.id(elementId)).size() > 0;
	}
	
	public boolean existsByXPath(WebDriver driver , String xPath){
		int size = driver.findElements(By.xpath(xPath)).size();
		System.out.println("size is="+size);
		return  size > 0;
	}
}
