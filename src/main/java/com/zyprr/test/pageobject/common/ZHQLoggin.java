package com.zyprr.test.pageobject.common ;

import java.util.Map ;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver ;
import org.openqa.selenium.WebElement ;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.CacheLookup ;
import org.openqa.selenium.support.FindBy ;
import org.openqa.selenium.support.PageFactory ;
import com.zyprr.test.pageobject.common.ZipperHQHome ;
import com.zyprr.test.utility.ZyprrUtilities ;

public class ZHQLoggin {

	private WebDriver	webDriver ;

	public ZHQLoggin(WebDriver webDriver) {
		this.webDriver = webDriver ;
	}

	@ CacheLookup
	@ FindBy(id = "username")
	WebElement	userName ;

	@ CacheLookup
	@ FindBy(id = "password-field")
	WebElement	password ;

	@ CacheLookup
	@ FindBy(id = "rememberMe1")
	WebElement	rememberMe ;

	@ CacheLookup
	@ FindBy(id = "zhqSignIn")
	WebElement	loginButton ;
	
	@ CacheLookup
	@ FindBy(id = "social_google_login")
	WebElement	socialGoogleLogin ;
	
 
	
	private void setUserName(
			String userName) {
		this.userName.sendKeys(userName) ;
	}

	private void setPassword(
			String password) {
		this.password.sendKeys(password) ;
	}

	private void pressRememberMe() {
		System.out.println("rememberMe button"+rememberMe);
		this.rememberMe.click() ;
	}

	private void pressLogin() {
		System.out.println("login button"+loginButton);
		this.loginButton.click() ;
	}

	
	
	public ZipperHQHome loginToZipperHQ(
			Map < String, Object > inputData, String [ ] fieldNames) {
		
		setUserName((String) inputData.get(fieldNames [0])) ;
		setPassword((String) inputData.get(fieldNames [1])) ;
		System.out.println("login buttonclickdsdsd");

		ZyprrUtilities.takeDriverSnapShot(webDriver, webDriver.getTitle()) ;
		
		pressLogin() ;
		
		
        System.out.println("login buttonclick");

		// ZyprrUtil.takeDriverSnapShot( webDriver , webDriver.getTitle( ) ) ;
		// return new ZyprrHomePageObject(webDriver) ;
		return PageFactory.initElements(webDriver, ZipperHQHome.class) ;
	}
	
	
	}
	

