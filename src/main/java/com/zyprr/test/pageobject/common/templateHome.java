package com.zyprr.test.pageobject.common;

import java.awt.datatransfer.StringSelection;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import java.awt.*;
import java.awt.event.KeyEvent;
import com.zyprr.test.base.factory.ZyprrPageFactory;

public class templateHome extends BaseHome {
	
	public templateHome(WebDriver webDriver) {
		super(webDriver);
	}
	
	@CacheLookup
	@FindBy(id = "templates")
	WebElement Templates;
	
	@CacheLookup
	@FindBy(id = "campaign_createtemplate_new")
	WebElement createNewTemplateBtn;
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"email-builder\"]/div[2]/div[3]/div[2]/ul/li[1]")
	WebElement text;
	
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"email-builder\"]/div[2]/div[3]/div[2]/ul/li[4]")
	WebElement video;
	

	@CacheLookup
	@FindBy(xpath = "//*[@id=\"email-builder\"]/div[2]/div[3]/div[2]/ul/li[16]/img")
	WebElement footer;
	
	@CacheLookup
	@FindBy(id = "emailHolder")
	WebElement emailHolderBox;
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"email-builder\"]/div[2]/div[3]/div[1]/ul/li[1]")
	WebElement createBtn;
	
	@CacheLookup
	@FindBy(id = "save-dropdown-toggle")
	WebElement saveDropdownToggle;
	

	@CacheLookup
	@FindBy(id = "Save-as-Quick-Send-Template")
	WebElement saveQuickSendTemplate;
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"email-builder\"]/div[2]/div[3]/div[1]/button[1]")
	WebElement saveQuickSendContinueBtn;
	
	@CacheLookup
	@FindBy(id = "template_name")
	WebElement templateNameTextBox;
	
	
	@CacheLookup
	@FindBy(id = "brief_description")
	WebElement briefDescriptionTextBox;
	
	@CacheLookup
	@FindBy(id = "favorite")
	WebElement favoriteBox;
	
	@CacheLookup
	@FindBy(id = "savetemplate_button")
	WebElement saveTemplateBtn;
	
	
	@CacheLookup
	@FindBy(id = "closeTemplateSavedDialog")
	WebElement closeTemplateSavedDialog;
	
	
	@CacheLookup
	@FindBy(id = "Save-as-Regular-Template")
	WebElement saveRegularTemplate;
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"camp_list_left_ul_li_pre\"]/a")
	WebElement preBuiltTemplates ;
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"camp_list_left_ul_li_my\"]/a")
	WebElement myTemplates ;
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"camp_list_left_ul_li_fav\"]/a")
	WebElement favorites ;
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"camp_list_left_ul_li_qstemp\"]/a")
	WebElement quickSendTemplates  ;
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"videoImg\"]")
	WebElement clickToAddvideo  ;
	
	@CacheLookup
	@FindBy(id = "selectVideoList")
	WebElement selectVideoBtn;
	
	@CacheLookup
	@FindBy(id = "video_uploader_up_btn")
	WebElement uploadVideoBtn;
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"email-builder\"]/div[2]/div[3]/div[2]/form/div[2]/div/div/button")
	WebElement recordVideoBtn;
	
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"video_selector_anim_gif_0\"]")
	WebElement videoSelectForList;
	
	@CacheLookup 
	@FindBy(id = "recorder_modal_play_btn")
	WebElement recordStartBtn;
	
	@CacheLookup        
	@FindBy(xpath = "//*[@id=\"recorderVMod\"]/div[7]/button[1]")
	WebElement recordStopBtn;
	
	@CacheLookup 
	@FindBy(id = "recorder_modal_save_btn")
	WebElement recorderSaveBtn;
	
	
	@CacheLookup 
	@FindBy(id = "recorder_modal_close_btn")
	WebElement recorderModalCloseBtn;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public templateHome dragAndDrop() throws InterruptedException {
		Thread.sleep(4000);
		createNewTemplateBtn.click();
		Thread.sleep(4000);
		Actions act1 = new Actions(webDriver);
		WebElement src1 = text;
        WebElement dest1 = emailHolderBox;
        act1.clickAndHold(src1)
                .moveToElement(dest1)
                .release()
                .build()
                .perform();
        Thread.sleep(2000);
        createBtn.click();
        Thread.sleep(2000);
        Actions act2 = new Actions(webDriver);
		WebElement src2 = video;
        WebElement dest2 = emailHolderBox;
        act1.clickAndHold(src2)
                .moveToElement(dest2)
                .release()
                .build()
                .perform();
        Thread.sleep(2000);
        createBtn.click();
        Thread.sleep(2000);
        Actions act3 = new Actions(webDriver);
		WebElement src3 = footer;
        WebElement dest3 = emailHolderBox;
        act1.clickAndHold(src3)
                .moveToElement(dest3)
                .release()
                .build()
                .perform();
		
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				templateHome.class);
	}
	
	public templateHome saveQuickSendTemplate() throws InterruptedException {
		Thread.sleep(4000);
		saveDropdownToggle.click();
		saveQuickSendTemplate.click();
		saveQuickSendContinueBtn.click();
		templateNameTextBox.sendKeys("Test Automation");
		briefDescriptionTextBox.sendKeys("One of the most beautiful qualities of true friendship is to understand and to be understood. ...");
		favoriteBox.click();
		saveTemplateBtn.click();
		Thread.sleep(20000);
		closeTemplateSavedDialog.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				templateHome.class);
	}
	

	public templateHome saveRegularTemplate() throws InterruptedException {
		Thread.sleep(4000);
		saveDropdownToggle.click();
		saveRegularTemplate.click();
		templateNameTextBox.sendKeys("Test Automation");
		briefDescriptionTextBox.sendKeys("One of the most beautiful qualities of true friendship is to understand and to be understood. ...");
		favoriteBox.click();
		saveTemplateBtn.click();
		Thread.sleep(20000);
		closeTemplateSavedDialog.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				templateHome.class);
	}
	
	public templateHome checkAllFilter() throws InterruptedException {
		Thread.sleep(4000);
		preBuiltTemplates.click();
		Thread.sleep(4000);
		myTemplates.click();
		Thread.sleep(4000);
		favorites.click();
		Thread.sleep(4000);
		quickSendTemplates.click();
		
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				templateHome.class);
	}
	
	public templateHome selectVideo() throws InterruptedException {
		Thread.sleep(4000);
		clickToAddvideo.click();
		selectVideoBtn.click();
		videoSelectForList.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				templateHome.class);
	}
	
	public templateHome uploadVideo() throws InterruptedException, AWTException {
		Thread.sleep(4000);
		clickToAddvideo.click();
		uploadVideoBtn.click();
		Thread.sleep(2000);
		 StringSelection ss = new StringSelection("/home/zyprr/Videos/adventure.mp4");
	        Thread.sleep(2000);
	        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
           Robot robot = new Robot();
	        robot.keyPress(KeyEvent.VK_CONTROL);
	        robot.keyPress(KeyEvent.VK_V);
	        robot.keyRelease(KeyEvent.VK_V);
	        robot.keyRelease(KeyEvent.VK_CONTROL);
	        robot.keyPress(KeyEvent.VK_ENTER);
	        robot.keyRelease(KeyEvent.VK_ENTER);
	        Thread.sleep(30000);
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				templateHome.class);
	}
	
	public templateHome recordVideo() throws InterruptedException {
		Thread.sleep(4000);
		clickToAddvideo.click();
		recordVideoBtn.click();
		recordStartBtn.click();
		Thread.sleep(5000);
		recordStopBtn.click();
		recorderSaveBtn.click();
		Thread.sleep(20000);
		recorderModalCloseBtn.click();
		
		
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				templateHome.class);
	}
	
	
	
	
	

	public templateHome createNewTemplate() throws InterruptedException {
		Thread.sleep(4000);
		createNewTemplateBtn.click();
		
		
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				templateHome.class);
	}
	
	
}