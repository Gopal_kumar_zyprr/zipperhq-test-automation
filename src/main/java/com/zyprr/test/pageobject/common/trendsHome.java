
package com.zyprr.test.pageobject.common;

import java.awt.datatransfer.StringSelection;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import java.awt.*;
import java.awt.event.KeyEvent;
import com.zyprr.test.base.factory.ZyprrPageFactory;

public class trendsHome extends BaseHome {
	
	public trendsHome(WebDriver webDriver) {
		super(webDriver);
	}

	@CacheLookup
	@FindBy(id = "trends_left_act_2")
	WebElement trendsContacts;
	
	
	
	@CacheLookup
	@FindBy(id = "trend_contact_list_name_0")
	WebElement detailsContacts;
	
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"camp_card_list_0\"]/div[2]/div/i")
	WebElement previewCampaigns ;
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"campOpenPreview\"]/div/div/div[1]/button")
	WebElement previewCloseBtn ;
	
	
	

	
	public trendsHome ContactDetails() throws InterruptedException {
		Thread.sleep(4000);
		trendsContacts.click();
		Thread.sleep(3000);
		detailsContacts.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				trendsHome.class);
	}
	

	public trendsHome preview() throws InterruptedException {
		Thread.sleep(4000);
		previewCampaigns.click();
		Thread.sleep(3000);
		previewCloseBtn.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				trendsHome.class);
	}

	
}