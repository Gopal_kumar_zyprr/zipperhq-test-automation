package com.zyprr.test.pageobject.common;

import java.awt.datatransfer.StringSelection;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import java.awt.*;
import java.awt.event.KeyEvent;
import com.zyprr.test.base.factory.ZyprrPageFactory;

public class DashboardHome extends BaseHome {
	
	public DashboardHome(WebDriver webDriver) {
		super(webDriver);
	}
	
	@CacheLookup
	@FindBy(id = "dashboard")
	WebElement dashboard;
	
	
	
	@CacheLookup
	@FindBy(id = "video_uploader_up_btn")
	WebElement uploadVideoBtn;
	
	
	@CacheLookup
	@FindBy(id = "dash_contact_show_anch")
	WebElement showAllContactsBtn;
	
	
	
	@CacheLookup
	@FindBy(id = "dash_camp_show_anch")
	WebElement showAllCampaingsBtn;
	
	
	
	@CacheLookup
	@FindBy(id = "dash_camp_new_btn")
	WebElement createNewBtn;   
	
	@CacheLookup
	@FindBy(id = "dash_recorder_btn")
	WebElement dashRecordVideoBtn;
	

	@CacheLookup
	@FindBy(id = "video_recorder_btn")
	WebElement recordVideoBtn;
	
	
	
	@CacheLookup
	@FindBy(id = "recorder_modal_play_btn")
	WebElement recordStartBtn;
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"recorderVMod\"]/div[7]/button[1]")
	WebElement recordResumeBtn;
	
	
	
	@CacheLookup
	@FindBy(id = "recorder_modal_save_btn")
	WebElement recordSaveBtn;
	
	
	
	@CacheLookup
	@FindBy(id = "recorder_modal_close_btn")
	WebElement recorderCloseBtn;
	
	
	
	@CacheLookup
	@FindBy(id = "recorder_modal_createcamp_btn")
	WebElement recordCreateCampaignBtn;
	
	
	
	
	
	public DashboardHome Uploadvideo() throws InterruptedException, AWTException {
		Thread.sleep(4000);
		uploadVideoBtn.click();
		Thread.sleep(000);
		 StringSelection ss = new StringSelection("/home/zyprr/Videos/adventure.mp4");
	        Thread.sleep(2000);
	        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
           Robot robot = new Robot();
	        robot.keyPress(KeyEvent.VK_CONTROL);
	        robot.keyPress(KeyEvent.VK_V);
	        robot.keyRelease(KeyEvent.VK_V);
	        robot.keyRelease(KeyEvent.VK_CONTROL);
	        robot.keyPress(KeyEvent.VK_ENTER);
	        robot.keyRelease(KeyEvent.VK_ENTER);
	        Thread.sleep(25000);
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				DashboardHome.class);
	}
	
	public DashboardHome Showallcontacts() throws InterruptedException
	{
		Thread.sleep(4000);
		showAllContactsBtn.click();
		
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				DashboardHome.class);
	}
	
	public DashboardHome Showallcampaings() throws InterruptedException
	{
		Thread.sleep(4000);
		showAllCampaingsBtn.click();
		
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				DashboardHome.class);
	}
	

	public DashboardHome Createnew() throws InterruptedException
	{
		Thread.sleep(4000);
		createNewBtn.click();
		
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				DashboardHome.class);
	}
	
	public DashboardHome Recordsave() throws InterruptedException
	{
		Thread.sleep(4000);
		dashRecordVideoBtn.click();
		Thread.sleep(2000);
		recordStartBtn.click();
		Thread.sleep(5000);
		recordResumeBtn.click();
		Thread.sleep(7000);
		recordSaveBtn.click();
		Thread.sleep(10000);
		
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				DashboardHome.class);
	}
	
	public DashboardHome recordCreateCampaign() throws InterruptedException {
		Thread.sleep(4000);
		recordCreateCampaignBtn.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				DashboardHome.class);
	}
	
}