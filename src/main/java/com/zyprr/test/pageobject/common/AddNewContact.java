package com.zyprr.test.pageobject.common;

import java.util.Map;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.zyprr.test.base.factory.ZyprrPageFactory;
import com.zyprr.test.config.Config;
import com.zyprr.test.pageobject.common.ZipperHQHome;
import com.zyprr.test.pageobject.contact.ContactDetails;
import com.zyprr.test.pageobject.contact.ContactsHome;
import com.zyprr.test.testcase.abs.AbstractTest;

public class AddNewContact extends AbstractTest {

	private Map<String, Object> inputData = null;

	private static final String[] fieldNames = {"Email", "First Name",
		    "Last Name", "Primary Phone",
			};

	private static final String sheetName = "addcontact";
	private static AddNewContact staticInstance = new AddNewContact(null);

	@Factory(dataProvider = "dataMethod")
	public AddNewContact(Map<String, Object> inputData) {
		this.inputData = inputData;
	}

	@DataProvider
	public static Object[][] dataMethod() throws Exception {
		return staticInstance.getData();
	}

	@Test(priority = 0)
	public void addContactTest() throws InterruptedException {

		ZyprrPageFactory.getInstance()
				.getPageObject(webDriver, ZipperHQHome.class)
				.xferToContactHomePage()
				.createNewContact(inputData, fieldNames);

//		ContactsHome contactHome = ZyprrPageFactory.getInstance()
//				.getPageObject(webDriver, ZipperAgentHome.class)
//				.xferToContactHomePage()
//				.searchContacts((String) inputData.get(fieldNames[1]));
//
//		Assert.assertTrue(contactHome.getFirstNameInList().getText()
//				.contains((String) inputData.get(fieldNames[1])));

//		ContactDetails contactDetails = contactHome.getContactDetails();
//
//		Map<Integer, WebElement> contactElements = contactDetails
//				.getContactElements();

//		for (Integer key : contactElements.keySet()) {
//
//			String valueFromScreen = contactElements.get(key).getText();
//			String valueFromExcel = (String) inputData.get(fieldNames[key]);
//
//			try {
//				Verify.verify(valueFromScreen.contains(valueFromExcel));
//			} catch (Exception e) {
//				ZyprrUtilities.writeReportLog(fieldNames[key], "FAIL",
//						(String) inputData.get(fieldNames[1]),
//						(String) inputData.get(fieldNames[3]), valueFromScreen,
//						valueFromExcel);
//			}
//		}
//		String valueFromExcel = (String) inputData.get(fieldNames[44]);
//		if ("Yes".equalsIgnoreCase(valueFromExcel)) {
//			String valueFromExcelShareWith = (String) inputData
//					.get(fieldNames[45]);
//			String[] shareWith = valueFromExcelShareWith.split(",");
//			for (int i = 1; i <= shareWith.length; i++) {
//				String valueFromScreen = contactDetails
//						.getShareWithDataFromScreen(i);
//				try {
//					Verify.verify(valueFromExcelShareWith
//							.contains(valueFromScreen));
//				} catch (Exception e) {
//					ZyprrUtilities.writeReportLog(fieldNames[45], "FAIL",
//							(String) inputData.get(fieldNames[1]),
//							(String) inputData.get(fieldNames[3]),
//							valueFromScreen, valueFromExcel);
//				}
//			}

//		}
		//contactDetails.closeBtn();

	}

	@Override
	protected String getDataPath() {

		return Config.getInstance().getContactDataPath();
	}

	@Override
	protected String getDataSheetName() {
		// TODO Auto-generated method stub
		return sheetName;
	}

	@Override
	protected String[] getFieldNames() {
		// TODO Auto-generated method stub
		return fieldNames;
	}

	

}