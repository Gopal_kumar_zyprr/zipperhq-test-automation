

package com.zyprr.test.pageobject.common;

import java.awt.datatransfer.StringSelection;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import java.awt.*;
import java.awt.event.KeyEvent;
import com.zyprr.test.base.factory.ZyprrPageFactory;

public class recordandsendHome extends BaseHome {
	
	public recordandsendHome(WebDriver webDriver) {
		super(webDriver);
	}
	
	
	@CacheLookup
	@FindBy(id = "videos")
	WebElement videos;
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"mediaDiv\"]/div[1]/div[1]/div/div[2]/div/div[2]/div[2]/div/ul/li[1]/div[3]/div[3]/button")
	WebElement moreDropDownBtn;
	
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"mediaDiv\"]/div[1]/div[1]/div/div[2]/div/div[2]/div[2]/div/ul/li[1]/div[3]/div[3]/ul/li[2]")
	WebElement videoEdit;
	
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"videoEditDialog\"]/div/div/div[2]/div/div[2]/div[4]/div/button")
	WebElement catEditBtn;
	
	
	
	@CacheLookup
	@FindBy(id = "video_recorder_btn")
	WebElement recordVideoBtn;
	
	
	
	@CacheLookup 
	@FindBy(id = "recorder_modal_play_btn")
	WebElement recordStartBtn;
	
	@CacheLookup        
	@FindBy(xpath = "//*[@id=\"recorderV\"]/div[7]/button[1]")
	WebElement recordStopBtn;
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"recorder_inline_play_overlay\"]/button")
	WebElement recordBtn;
	
	@CacheLookup
	@FindBy(id = "recsend_act_template_select")
	WebElement templateBtn;
	
	@CacheLookup
	@FindBy(id = "qs_temp_sel_thumb_url_0")
	WebElement template1;
	
	@CacheLookup
	@FindBy(id = "qs_temp_search_inp")
	WebElement templateSearchBox;
	
	
	@CacheLookup
	@FindBy(id = "recsend_act_sub_input")
	WebElement subjectTextBox;
	
	
	
	@CacheLookup     
	@FindBy(xpath = "//*[@id=\"textQS_placeholder\"]/textarea")
	WebElement addTextBox;
	
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"videoRecAnc_zipper\"]/videorecorder/div[1]/a/i")
	WebElement recordVideoBoxclose;
	
	

	@CacheLookup
	@FindBy(xpath = "//*[@id=\"recordAndSendDialogHead\"]/div/div/div[2]/div[6]/div[2]/div/label")
	WebElement emailOpensNotifyCheckBox;
	
	
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"recordAndSendDialogHead\"]/div/div/div[2]/div[6]/div[3]/div/label")
	WebElement videoPlaysNotifyCheckBox;
	
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"recordAndSendDialogHead\"]/div/div/div[2]/div[3]/div[2]/div[2]/label")
	WebElement listCheckBox;
	
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"recordAndSendDialogHead\"]/div/div/div[2]/div[3]/div[2]/div[1]/label")
	WebElement emailCheckBox;
	
	@CacheLookup
	@FindBy(id = "recsend_act_list_auto")
	WebElement listName;
	
	
	
	@CacheLookup
	@FindBy(id = "recsend_act_send_btn")
	WebElement sendBtn;
	
	@CacheLookup
	@FindBy(id = "recsend_act_saveexit_btn")
	WebElement saveExitBtn;
	

	@CacheLookup
	@FindBy(id = "recsend_act_email_auto")
	WebElement toTextBox;
	
	@CacheLookup
	@FindBy(id = "rec_comp_addr_street")
	WebElement streetName;
	
	
	
	@CacheLookup
	@FindBy(id = "rec_comp_addr_unit")
	WebElement unit;
	
	@CacheLookup
	@FindBy(id = "rec_comp_addr_city")
	WebElement cityName;
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"saveCampaignModal\"]/div/div/div[2]/div/div/div[2]/div[4]/select")
	WebElement dropDownState;
	
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"saveCampaignModal\"]/div/div/div[2]/div/div/div[2]/div[4]/select/option[1]")
	WebElement dropDownNone;
	
	
	@CacheLookup
	@FindBy(id = "rec_comp_addr_zip")
	WebElement zipCode;
	
	
	@CacheLookup
	@FindBy(id = "recp_comp_a_email")
	WebElement companyEmail;
	
	@CacheLookup
	@FindBy(id = "recp_comp_a_phone")
	WebElement companyNumber;
	
	
	
	@CacheLookup
	@FindBy(id = "recsend_save_camp_send_btn")
	WebElement sendAndSave;     
	
	@CacheLookup
	@FindBy(id = "recsend_act_close_btn")
	WebElement recordSendBoxClose;
	
	
	
	
	@CacheLookup
	@FindBy(id = "recsend_act_med_discard_btn")
	WebElement discardBtn;
	
	
	@CacheLookup
	@FindBy(id = "recsend_act_video_select")
	WebElement videoSelectBtn;
	
	
	@CacheLookup
	@FindBy(id = "video_selector_anim_gif_0")
	WebElement videoSelect;
	
	
	

	@CacheLookup
	@FindBy(xpath = "//*[@id=\"recordAndSendDialogHead\"]/div/div/div[2]/div[4]/div[2]/div/label")
	WebElement ctaToggle;
	
	
	
	@CacheLookup
	@FindBy(id = "ctaDescription_textBox_recordAndSend") 
	WebElement ctaDescription;
	
	
	@CacheLookup
	@FindBy(id = "ctaLink_textBox_recordAndSend")
	WebElement ctaLink;
	
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"callToActionModal\"]/div/div/div[2]/div[2]/button[2]")
	WebElement ctaOkBtn;
	
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"callToActionModal\"]/div/div/div[2]/div[2]/button[1]")
	WebElement ctaCancelBtn;
	
	
	
	@CacheLookup
	@FindBy(id = "callToActionModal_close_btn")
	WebElement ctaXBtn;
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"ctaDeleteAlert_recordAndSend\"]/div/div/div[2]/button[2]")
	WebElement ctaDeleteYesBtn;
	
	
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"ctaDeleteAlert_recordAndSend\"]/div/div/div[2]/button[1]")
	WebElement ctaDeleteNoBtn;
	
	
	
	
	
	
	public recordandsendHome recordVideo() throws InterruptedException {
		Thread.sleep(2000);
		recordBtn.click();
		Thread.sleep(7000);
		recordStopBtn.click();
		Thread.sleep(10000);
		templateBtn.click();
		Thread.sleep(4000);
		templateSearchBox.sendKeys("Happy Birthday");
		Thread.sleep(2000);
		WebElement textbox1 = templateSearchBox;
        textbox1.sendKeys(Keys.ENTER);
        Thread.sleep(4000);
        template1.click();
        Thread.sleep(2000);
		subjectTextBox.clear();
		subjectTextBox.sendKeys("Test Record And Send");
		Thread.sleep(2000);
		addTextBox.sendKeys("ZipperHQ makes it quick, easy and affordable to add personalized videos to your email, social and SMS communications.");
		Thread.sleep(2000);
		emailOpensNotifyCheckBox.click();
		videoPlaysNotifyCheckBox.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				recordandsendHome.class);
	}
	
	
	
	
	
	
	
	public recordandsendHome textSend() throws InterruptedException {
		Thread.sleep(2000);
		recordVideoBoxclose.click();
		Thread.sleep(2000);
		templateBtn.click();
		Thread.sleep(4000);
		templateSearchBox.sendKeys("Happy Birthday");
		Thread.sleep(2000);
		WebElement textbox1 = templateSearchBox;
        textbox1.sendKeys(Keys.ENTER);
        Thread.sleep(4000);
        template1.click();
        Thread.sleep(2000);
		subjectTextBox.clear();
		subjectTextBox.sendKeys("Test Record And Send");
		Thread.sleep(2000);
		addTextBox.sendKeys("ZipperHQ makes it quick, easy and affordable to add personalized videos to your email, social and SMS communications.");
		Thread.sleep(2000);
		emailOpensNotifyCheckBox.click();
		videoPlaysNotifyCheckBox.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				recordandsendHome.class);
	}
	public recordandsendHome toEmail() throws InterruptedException {
		Thread.sleep(2000);
		listCheckBox.click();
		emailCheckBox.click();
		Thread.sleep(2000);
		toTextBox.sendKeys("gopalkumaratwork@gmail.com");
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				recordandsendHome.class);
	}
	
	public recordandsendHome toList() throws InterruptedException {
		Thread.sleep(2000);
		listCheckBox.click();
		emailCheckBox.click();
		listCheckBox.click();
		Thread.sleep(2000);
		listName.sendKeys("Friend");
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				recordandsendHome.class);
	}
	
	
	public recordandsendHome discard() throws InterruptedException {
		Thread.sleep(2000);
		recordSendBoxClose.click();
		Thread.sleep(3000);
		discardBtn.click();
		
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				recordandsendHome.class);
	}
	
	public recordandsendHome send() throws InterruptedException {
		Thread.sleep(2000);
		sendBtn.click();
		Thread.sleep(2000);
		streetName.clear();
		streetName.sendKeys("12345");
		unit.clear();
		unit.sendKeys("xxxx");
		cityName.clear();
		cityName.sendKeys("Kolkata");
		Thread.sleep(2000);
		dropDownState.click();
		Thread.sleep(2000);
		dropDownNone.click();
		Thread.sleep(2000);
		zipCode.clear();
		zipCode.sendKeys("123456");
		companyEmail.clear();
		companyEmail.sendKeys("gopal.kumar@zyprr.com");
		companyNumber.clear();
		companyNumber.sendKeys("7898745667");
		Thread.sleep(2000);
		sendAndSave.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				recordandsendHome.class);
	}
	
	
	
	public recordandsendHome selectVideo() throws InterruptedException {
		Thread.sleep(2000);
		videoSelectBtn.click();
		Thread.sleep(2000);
		videoSelect.click();
		Thread.sleep(2000);
		templateBtn.click();
		Thread.sleep(4000);
		templateSearchBox.sendKeys("Happy Birthday");
		Thread.sleep(2000);
		WebElement textbox1 = templateSearchBox;
        textbox1.sendKeys(Keys.ENTER);
        Thread.sleep(4000);
        template1.click();
        Thread.sleep(2000);
        subjectTextBox.clear();
		subjectTextBox.sendKeys("Test Record And Send");
		Thread.sleep(2000);
		addTextBox.sendKeys("ZipperHQ makes it quick, easy and affordable to add personalized videos to your email, social and SMS communications.");
		Thread.sleep(2000);
		emailOpensNotifyCheckBox.click();
		videoPlaysNotifyCheckBox.click();
		
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				recordandsendHome.class);
	}

	public recordandsendHome saveAndExit() throws InterruptedException {
		Thread.sleep(2000);
		saveExitBtn.click();
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				recordandsendHome.class);
	}
	
	public recordandsendHome companyDetails() throws InterruptedException {
		Thread.sleep(2000);
		
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				recordandsendHome.class);
	}
	
	public recordandsendHome cta() throws InterruptedException {
		Thread.sleep(4000);
		ctaToggle.click();
		Thread.sleep(4000);
		ctaDescription.clear();
		ctaDescription.sendKeys("Hurry!!!!!!!");
		Thread.sleep(4000);
		ctaLink.clear();
		ctaLink.sendKeys("https://stephen.zenzuzu.com/");
		Thread.sleep(4000);
		ctaCancelBtn.click();
		Thread.sleep(2000);
		ctaToggle.click();
		Thread.sleep(2000);
		ctaXBtn.click();
		Thread.sleep(2000);
		ctaToggle.click();
		Thread.sleep(2000);
		ctaOkBtn.click();
		
		
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				recordandsendHome.class);
	}
	
	public recordandsendHome ctaEdit() throws InterruptedException {
		Thread.sleep(2000);
		videos.click();
		Thread.sleep(2000);
		moreDropDownBtn.click();
		Thread.sleep(2000);
		videoEdit.click();
		Thread.sleep(2000);
		catEditBtn.click();
		Thread.sleep(2000);
		ctaDescription.clear();
		ctaDescription.sendKeys("Hurry!");
		ctaLink.clear();
		ctaLink.sendKeys("https://www.zyprr.com/");
		ctaCancelBtn.click();
		Thread.sleep(2000);
		ctaToggle.click();
		Thread.sleep(2000);
		ctaXBtn.click();
		Thread.sleep(2000);
		ctaToggle.click();
		Thread.sleep(2000);
		ctaOkBtn.click();
		
		
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				recordandsendHome.class);
	}
	
	public recordandsendHome ctaDelete() throws InterruptedException {
		Thread.sleep(2000);
		ctaToggle.click();
		ctaDeleteNoBtn.click();
		Thread.sleep(2000);
		ctaToggle.click();
		Thread.sleep(2000);
		ctaDeleteYesBtn.click();
		
		
		
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				recordandsendHome.class);
	}
	


	
}