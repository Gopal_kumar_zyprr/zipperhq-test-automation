package com.zyprr.test.pageobject.common;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.zyprr.test.constants.Constants;

public class WaitUtil {

	private static WaitUtil instance = new WaitUtil();

	public WaitUtil() {
	}

	public static WaitUtil getInstance() {
		return instance;
	}

	public void waitForTextTobeFilled(WebDriver webDriver,
			WebElement webElement, String text) {
		WebDriverWait wait = new WebDriverWait(webDriver,
				Constants.WAIT_IN_SECONDS);
		wait.until(ExpectedConditions
				.textToBePresentInElement(webElement, text));
	}

	public void waitForElementTobeVisible(WebDriver webDriver,
			WebElement webElement) {
		WebDriverWait wait = new WebDriverWait(webDriver,
				Constants.WAIT_IN_SECONDS);

		JavascriptExecutor js = (JavascriptExecutor) webDriver;
		if (js.executeScript("return document.readyState").toString()
				.equals("complete")) {
			wait.until(ExpectedConditions.visibilityOf(webElement));
		}
	}

	public void waitForElementTobeClickable(WebDriver webDriver,
			WebElement webElement) {
		WebDriverWait wait = new WebDriverWait(webDriver,
				Constants.WAIT_IN_SECONDS);
		wait.until(ExpectedConditions.elementToBeClickable(webElement));
	}

	public void waitForElementSelectionStateToBe(WebDriver webDriver,
			WebElement webElement, boolean selected) {
		WebDriverWait wait = new WebDriverWait(webDriver,
				Constants.WAIT_IN_SECONDS);
		wait.until(ExpectedConditions.elementSelectionStateToBe(webElement,
				selected));
	}

	public void waitForPageToRefreshAndElementTobeVisible(WebDriver webDriver,
			WebElement webElement) {
		WebDriverWait wait = new WebDriverWait(webDriver,
				Constants.WAIT_IN_SECONDS);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions
				.visibilityOf(webElement)));
	}

	public void waitForElementTobeInVisible(WebDriver webDriver,
			WebElement webElement) {
		WebDriverWait wait = new WebDriverWait(webDriver,
				Constants.WAIT_IN_SECONDS);
		wait.until(ExpectedConditions.not(ExpectedConditions
				.visibilityOf(webElement)));
	}
}
