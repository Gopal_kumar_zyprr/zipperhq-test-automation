package com.zyprr.test.pageobject.common;

import java.awt.datatransfer.StringSelection;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import java.awt.*;
import java.awt.event.KeyEvent;
import com.zyprr.test.base.factory.ZyprrPageFactory;

public class AdminHome extends BaseHome {
	
	public AdminHome(WebDriver webDriver) {
		super(webDriver);
	}
	
	@CacheLookup
	@FindBy(id = "userProfileInitials")
	WebElement userIcon;
	
	@CacheLookup
	@FindBy(id = "HQ_profile-Settings")
	WebElement profileSettings;
	
	@CacheLookup
	@FindBy(id = "usr_det_open")
	WebElement profile;
	
	@CacheLookup
	@FindBy(id = "usr_btn_chng")
	WebElement changePasswordBtn;
	
	@CacheLookup
	@FindBy(id = "oldPassword")
	WebElement oldPassword;
	
	@CacheLookup
	@FindBy(id = "password")
	WebElement newPassword;
	
	@CacheLookup
	@FindBy(id = "confirmPassword")
	WebElement confirmPassword;
	
	
	@CacheLookup
	@FindBy(id = "submit")
	WebElement changeBtn;
	
	@CacheLookup
	@FindBy(id = "returnBttn")
	WebElement returnBttn;
	
	@CacheLookup
	@FindBy(xpath = "//*[@id=\"box_wrapper\"]/section/article/div/div[2]/div[2]/a")
	WebElement companyDetailsSettings;
	
	
	
	public AdminHome changePassword() throws InterruptedException
	{
		Thread.sleep(4000);
		profile.click();
		changePasswordBtn.click();
		oldPassword.sendKeys("zipperhq13");
		newPassword.sendKeys("zipperhq1234");
		confirmPassword.sendKeys("zipperhq1234");
		changeBtn.click();
		Thread.sleep(2000);
		returnBttn.click();
		
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				AdminHome.class);
	}
	
	public AdminHome profile() throws InterruptedException
	{
		Thread.sleep(4000);
		companyDetailsSettings.click();
		
		
		return ZyprrPageFactory.getInstance().getPageObject(webDriver,
				AdminHome.class);
	}
	
	
	
	
	
	
	
	
}

