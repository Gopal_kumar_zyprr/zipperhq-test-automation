package com.zyprr.test.pageobject.contact;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import com.zyprr.test.base.factory.ZyprrPageFactory;
import com.zyprr.test.pageobject.common.BaseHome;
import com.zyprr.test.pageobject.util.WaitUtil;
import com.zyprr.test.utility.ZyprrUtilities;
	

	public class ContactsHome extends BaseHome {

	

		public ContactsHome(WebDriver webDriver) {
			super(webDriver);
		}
		
		@CacheLookup
		@FindBy(id = "contacts")
		WebElement Contacts;

		@CacheLookup
		@FindBy(id = "lH_contact_bttns_new")
		WebElement quickAddButton;
		
		@CacheLookup
		@FindBy(id = "lH_contact_srch_input")
		WebElement filterContactsTextBox;
		
		@CacheLookup
		@FindBy(id = "lH_contact_srch_icon")
		WebElement filterContactsSearchBtn;
		
		@CacheLookup
		@FindBy(xpath = "//*[@id=\"contact_td_data_contactNames_0\"]/span")
		WebElement firstNameInList;
		
		@CacheLookup
		@FindBy(xpath = "//*[@id=\"lH_contact_art_ftr\"]/div/div[1]/h4/i")
		WebElement contactHomeTable;
		
		public AddContacts addContactButton() throws InterruptedException {
			Thread.sleep(5000);
			this.quickAddButton.click();
			ZyprrUtilities.takeDriverSnapShot(webDriver, webDriver.getTitle());
			return ZyprrPageFactory.getInstance().getPageObject(webDriver,
					AddContacts.class);
			
			
		}
		public WebElement getFirstNameInList() {
			//	WebElement elem = webDriver.findElement(By.id("contact_td_data_contactNames_0"));
			//	System.out.println("elem value is for assertion= "+elem);
				return firstNameInList;
			}
		
		private ContactsHome setfilterContactsTextBox(String firstName) throws InterruptedException {
			
			Thread.sleep(2000);
			this.filterContactsTextBox.clear();
			Thread.sleep(2000);
			this.filterContactsTextBox.sendKeys(firstName);

			return this;
		}
		
		public ContactsHome searchContacts(String searchString)
				throws InterruptedException {
			Thread.sleep(3000) ;
			setfilterContactsTextBox(searchString);
			filterContactsSearchBtn.click();
			WaitUtil.getInstance().waitForElementTobeVisible(webDriver,
					contactHomeTable);
			return ZyprrPageFactory.getInstance().getPageObject(webDriver,
					ContactsHome.class);
		}


}
